###### Acolyte Estate Privilege

estate_acolytes_dominion_laws = {
	icon = privilege_dominion_law
	influence = 0
	loyalty = 0
	max_absolutism = 0
	is_valid = {
		is_subject_of_type = acolyte_dominion
	}
	can_select = {
		always = yes
	}
	can_revoke = {
		always = no
	}
	on_revoked = {
	}
	on_granted = {
	}
	penalties = {
	}
	benefits = {
	}
	mechanics = {
	}
	
	conditional_modifier = {
		trigger = {
			overlord = { has_estate_privilege = estate_acolytes_dominions_law_army_decentralisation }
		}
		
		modifier = {
			land_forcelimit_modifier = 0.2
			land_maintenance_modifier = -0.1
		}
		
		is_bad = no
	}
	
	conditional_modifier = {
		trigger = {
			overlord = { has_estate_privilege = estate_acolytes_dominions_law_army_centralisation }
		}
		
		modifier = {
			land_forcelimit_modifier = -0.2
			land_maintenance_modifier = 0.1
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			overlord = { has_estate_privilege = estate_acolytes_dominions_law_the_dark_tithe }
		}
		
		modifier = {
			global_tax_modifier = -0.2
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			overlord = { has_estate_privilege = estate_acolytes_dominions_law_centralized_banking }
		}
		
		modifier = {
			interest = -1
		}
		
		is_bad = no
	}
	
	conditional_modifier = {
		trigger = {
			overlord = { has_estate_privilege = estate_acolytes_dominions_law_power_split }
		}
		
		modifier = {
			land_morale = 0.075
			discipline = 0.025
		}
		
		is_bad = no
	}
	
	
	ai_will_do = {
		factor = 1000
	}
}

estate_acolytes_land_share_10 = {
	icon = privilege_land_share_10
	influence = 0.2
	loyalty = 0.1
	max_absolutism = -5
	is_valid = {
		tag = Z99
		OR = {
			has_estate_privilege = estate_acolytes_land_share_10
			has_estate_privilege = estate_acolytes_land_share_20
		}
	}
	can_select = {
		has_estate_privilege = estate_acolytes_land_share_20
	}
	can_revoke = {
		always = no
	}
	on_revoked = {
	}
	on_granted = {
		if = {
			limit = { has_estate_privilege = estate_acolytes_land_share_20 }
			remove_estate_privilege = estate_acolytes_land_share_20
			every_subject_country = {
				limit = { is_subject_of_type = acolyte_dominion }
				add_liberty_desire = -5
			}
		}
	}
	penalties = {
	}
	benefits = {
		reduced_liberty_desire = 10
	}
	mechanics = {
	}
	ai_will_do = {
		factor = 1
	}
}

estate_acolytes_land_share_20 = {
	icon = privilege_land_share_20
	influence = 0.1
	loyalty = 0.05
	is_valid = {
		tag = Z99
		OR = {
			has_estate_privilege = estate_acolytes_land_share_10
			has_estate_privilege = estate_acolytes_land_share_20
			has_estate_privilege = estate_acolytes_land_share_30
		}
	}
	can_select = {
		OR = {
			has_estate_privilege = estate_acolytes_land_share_10
			has_estate_privilege = estate_acolytes_land_share_30
		}
	}
	can_revoke = {
		always = no
	}
	on_revoked = {
	}
	on_granted = {
		if = {
			limit = { has_estate_privilege = estate_acolytes_land_share_10 }
			remove_estate_privilege = estate_acolytes_land_share_10
			every_subject_country = {
				limit = { is_subject_of_type = acolyte_dominion }
				add_liberty_desire = 10
			}
		}
		else_if = {
			limit = { has_estate_privilege = estate_acolytes_land_share_30 }
			remove_estate_privilege = estate_acolytes_land_share_30
			every_subject_country = {
				limit = { is_subject_of_type = acolyte_dominion }
				add_liberty_desire = -5
			}
		}
	}
	penalties = {
		reduced_liberty_desire = -5
	}
	benefits = {
	}
	mechanics = {
	}
	ai_will_do = {
		factor = 1
	}
}

estate_acolytes_land_share_30 = {
	icon = privilege_land_share_30
	influence = 0.05
	loyalty = -0.05
	max_absolutism = 5
	is_valid = {
		tag = Z99
		OR = {
			has_estate_privilege = estate_acolytes_land_share_20
			has_estate_privilege = estate_acolytes_land_share_30
			has_estate_privilege = estate_acolytes_land_share_40
		}
	}
	can_select = {
		OR = {
			has_estate_privilege = estate_acolytes_land_share_20
			has_estate_privilege = estate_acolytes_land_share_40
		}
	}
	can_revoke = {
		always = no
	}
	on_revoked = {
	}
	on_granted = {
		if = {
			limit = { has_estate_privilege = estate_acolytes_land_share_20 }
			remove_estate_privilege = estate_acolytes_land_share_20
			every_subject_country = {
				limit = { is_subject_of_type = acolyte_dominion }
				add_liberty_desire = 10
			}
		}
		else_if = {
			limit = { has_estate_privilege = estate_acolytes_land_share_40 }
			remove_estate_privilege = estate_acolytes_land_share_40
			every_subject_country = {
				limit = { is_subject_of_type = acolyte_dominion }
				add_liberty_desire = -5
			}
		}
	}
	penalties = {
		reduced_liberty_desire = -20
	}
	benefits = {
	}
	mechanics = {
	}
	ai_will_do = {
		factor = 1
	}
}

estate_acolytes_land_share_40 = {
	icon = privilege_land_share_40
	influence = -0.05
	loyalty = -0.10
	max_absolutism = 10
	is_valid = {
		tag = Z99
		OR = {
			has_estate_privilege = estate_acolytes_land_share_30
			has_estate_privilege = estate_acolytes_land_share_40
			has_estate_privilege = estate_acolytes_land_share_50
		}
	}
	can_select = {
		OR = {
			has_estate_privilege = estate_acolytes_land_share_30
			has_estate_privilege = estate_acolytes_land_share_50
		}
	}
	can_revoke = {
		always = no
	}
	on_revoked = {
	}
	on_granted = {
		if = {
			limit = { has_estate_privilege = estate_acolytes_land_share_30 }
			remove_estate_privilege = estate_acolytes_land_share_30
			every_subject_country = {
				limit = { is_subject_of_type = acolyte_dominion }
				add_liberty_desire = 10
			}
		}
		else_if = {
			limit = { has_estate_privilege = estate_acolytes_land_share_50 }
			remove_estate_privilege = estate_acolytes_land_share_50
			every_subject_country = {
				limit = { is_subject_of_type = acolyte_dominion }
				add_liberty_desire = -5
			}
		}
	}
	penalties = {
		reduced_liberty_desire = -30
	}
	benefits = {
	}
	mechanics = {
	}
	ai_will_do = {
		factor = 1
	}
}

estate_acolytes_land_share_50 = {
	icon = privilege_land_share_50
	influence = -0.15
	loyalty = -0.15
	max_absolutism = 20
	is_valid = {
		tag = Z99
		OR = {
			has_estate_privilege = estate_acolytes_land_share_40
			has_estate_privilege = estate_acolytes_land_share_50
		}
	}
	can_select = {
		has_estate_privilege = estate_acolytes_land_share_40
	}
	can_revoke = {
		always = no
	}
	on_revoked = {
	}
	on_granted = {
		every_subject_country = {
			limit = { is_subject_of_type = acolyte_dominion }
			add_liberty_desire = 10
		}
		remove_estate_privilege = estate_acolytes_land_share_40
	}
	penalties = {
		reduced_liberty_desire = -50
	}
	benefits = {
	}
	mechanics = {
	}
	ai_will_do = {
		factor = 1
	}
}

estate_acolytes_the_black_host = {
	icon = privilege_the_dark_host
	influence = 0
	loyalty = 0
	is_valid = {
		tag = Z99
	}
	can_select = {
		always = yes
	}
	can_revoke = {
		always = no
	}
	on_revoked = {
	}
	on_granted = {
	}
	penalties = {
	}
	benefits = {
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 1
			NOT = { acolyte_dominion = 2 }
		}
		
		modifier = {
			global_tax_modifier = 0.05
			land_forcelimit_modifier = 0.05
			global_manpower_modifier = 0.05
		}
		
		is_bad = no
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 2
			NOT = { acolyte_dominion = 3 }
		}
		
		modifier = {
			global_tax_modifier = 0.1
			land_forcelimit_modifier = 0.1
			global_manpower_modifier = 0.1
		}
		
		is_bad = no
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 3
			NOT = { acolyte_dominion = 4 }
		}
		
		modifier = {
			global_tax_modifier = 0.15
			land_forcelimit_modifier = 0.15
			global_manpower_modifier = 0.15
		}
		
		is_bad = no
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 4
			NOT = { acolyte_dominion = 5 }
		}
		
		modifier = {
			global_tax_modifier = 0.2
			land_forcelimit_modifier = 0.2
			global_manpower_modifier = 0.2
		}
		
		is_bad = no
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 5
			NOT = { acolyte_dominion = 6 }
		}
		
		modifier = {
			global_tax_modifier = 0.25
			land_forcelimit_modifier = 0.25
			global_manpower_modifier = 0.25
		}
		
		is_bad = no
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 6
			NOT = { acolyte_dominion = 7 }
		}
		
		modifier = {
			global_tax_modifier = 0.30
			land_forcelimit_modifier = 0.30
			global_manpower_modifier = 0.30
		}
		
		is_bad = no
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 7
		}
		
		modifier = {
			global_tax_modifier = 0.35
			land_forcelimit_modifier = 0.35
			global_manpower_modifier = 0.35
		}
		
		is_bad = no
	}
	
	conditional_modifier = {
		trigger = {
			calc_true_if = {
				all_subject_country = {
					is_subject_of_type = acolyte_dominion
					liberty_desire = 50
				}
				amount = 1
			}
			NOT = {
				calc_true_if = {
					all_subject_country = {
						is_subject_of_type = acolyte_dominion
						liberty_desire = 50
					}
					amount = 2
				}
			}
		}
		
		modifier = {
			administrative_efficiency = -0.1
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			calc_true_if = {
				all_subject_country = {
					is_subject_of_type = acolyte_dominion
					liberty_desire = 50
				}
				amount = 2
			}
			NOT = {
				calc_true_if = {
					all_subject_country = {
						is_subject_of_type = acolyte_dominion
						liberty_desire = 50
					}
					amount = 3
				}
			}
		}
		
		modifier = {
			administrative_efficiency = -0.2
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			calc_true_if = {
				all_subject_country = {
					is_subject_of_type = acolyte_dominion
					liberty_desire = 50
				}
				amount = 3
			}
			NOT = {
				calc_true_if = {
					all_subject_country = {
						is_subject_of_type = acolyte_dominion
						liberty_desire = 50
					}
					amount = 4
				}
			}
		}
		
		modifier = {
			administrative_efficiency = -0.3
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			calc_true_if = {
				all_subject_country = {
					is_subject_of_type = acolyte_dominion
					liberty_desire = 50
				}
				amount = 4
			}
			NOT = {
				calc_true_if = {
					all_subject_country = {
						is_subject_of_type = acolyte_dominion
						liberty_desire = 50
					}
					amount = 5
				}
			}
		}
		
		modifier = {
			administrative_efficiency = -0.4
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			calc_true_if = {
				all_subject_country = {
					is_subject_of_type = acolyte_dominion
					liberty_desire = 50
				}
				amount = 5
			}
			NOT = {
				calc_true_if = {
					all_subject_country = {
						is_subject_of_type = acolyte_dominion
						liberty_desire = 50
					}
					amount = 6
				}
			}
		}
		
		modifier = {
			administrative_efficiency = -0.5
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			calc_true_if = {
				all_subject_country = {
					is_subject_of_type = acolyte_dominion
					liberty_desire = 50
				}
				amount = 6
			}
			NOT = {
				calc_true_if = {
					all_subject_country = {
						is_subject_of_type = acolyte_dominion
						liberty_desire = 50
					}
					amount = 7
				}
			}
		}
		
		modifier = {
			administrative_efficiency = -0.6
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			calc_true_if = {
				all_subject_country = {
					is_subject_of_type = acolyte_dominion
					liberty_desire = 50
				}
				amount = 7
			}
		}
		
		modifier = {
			administrative_efficiency = -0.7
		}
		
		is_bad = yes
	}
	
	mechanics = {
	}
	ai_will_do = {
		factor = 1
	}
}

estate_acolytes_dominions_law_army_decentralisation = {
	icon = privilege_dominion_law_army_decentralisation
	influence = 0.05
	loyalty = 0.05
	max_absolutism = -5
	is_valid = {
		tag = Z99
	}
	can_select = {
		NOT = { has_estate_privilege = estate_acolytes_dominions_law_army_centralisation }
	}
	can_revoke = {
		is_at_war = no
	}
	on_revoked = {
		every_subject_country = {
			limit = { is_subject_of_type = acolyte_dominion }
			add_liberty_desire = 10
		}
	}
	on_granted = {
		every_subject_country = {
			limit = { is_subject_of_type = acolyte_dominion }
			add_liberty_desire = -5
		}
	}
	penalties = {
	}
	
	benefits = {
		reduced_liberty_desire = 10
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 1
			NOT = { acolyte_dominion = 2 }
		}
		
		modifier = {
			land_forcelimit_modifier = -0.05
			land_maintenance_modifier = 0.025
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 2
			NOT = { acolyte_dominion = 3 }
		}
		
		modifier = {
			land_forcelimit_modifier = -0.1
			land_maintenance_modifier = 0.05
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 3
			NOT = { acolyte_dominion = 4 }
		}
		
		modifier = {
			land_forcelimit_modifier = -0.15
			land_maintenance_modifier = 0.075
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 4
			NOT = { acolyte_dominion = 5 }
		}
		
		modifier = {
			land_forcelimit_modifier = -0.2
			land_maintenance_modifier = 0.1
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 5
			NOT = { acolyte_dominion = 6 }
		}
		
		modifier = {
			land_forcelimit_modifier = -0.25
			land_maintenance_modifier = 0.125
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 6
			NOT = { acolyte_dominion = 7 }
		}
		
		modifier = {
			land_forcelimit_modifier = -0.3
			land_maintenance_modifier = 0.15
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 7
		}
		
		modifier = {
			land_forcelimit_modifier = -0.35
			land_maintenance_modifier = 0.175
		}
		
		is_bad = yes
	}
	
	mechanics = {
	}
	
	ai_will_do = {
		factor = 1
	}
}

estate_acolytes_dominions_law_army_centralisation = {
	icon = privilege_dominion_law_army_centralisation
	influence = -0.05
	loyalty = -0.05
	max_absolutism = 5
	is_valid = {
		tag = Z99
	}
	can_select = {
		NOT = { has_estate_privilege = estate_acolytes_dominions_law_army_decentralisation }
	}
	can_revoke = {
		is_at_war = no
	}
	on_revoked = {
		every_subject_country = {
			limit = { is_subject_of_type = acolyte_dominion }
			add_liberty_desire = -5
		}
	}
	on_granted = {
		every_subject_country = {
			limit = { is_subject_of_type = acolyte_dominion }
			add_liberty_desire = 10
		}
	}
	penalties = {
		reduced_liberty_desire = -10
	}
	
	benefits = {
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 1
			NOT = { acolyte_dominion = 2 }
		}
		
		modifier = {
			land_forcelimit_modifier = 0.05
			land_maintenance_modifier = -0.025
		}
		
		is_bad = no
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 2
			NOT = { acolyte_dominion = 3 }
		}
		
		modifier = {
			land_forcelimit_modifier = 0.1
			land_maintenance_modifier = -0.05
		}
		
		is_bad = no
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 3
			NOT = { acolyte_dominion = 4 }
		}
		
		modifier = {
			land_forcelimit_modifier = 0.15
			land_maintenance_modifier = -0.075
		}
		
		is_bad = no
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 4
			NOT = { acolyte_dominion = 5 }
		}
		
		modifier = {
			land_forcelimit_modifier = 0.2
			land_maintenance_modifier = -0.1
		}
		
		is_bad = no
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 5
			NOT = { acolyte_dominion = 6 }
		}
		
		modifier = {
			land_forcelimit_modifier = 0.25
			land_maintenance_modifier = -0.125
		}
		
		is_bad = no
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 6
			NOT = { acolyte_dominion = 7 }
		}
		
		modifier = {
			land_forcelimit_modifier = 0.3
			land_maintenance_modifier = -0.15
		}
		
		is_bad = no
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 7
		}
		
		modifier = {
			land_forcelimit_modifier = 0.35
			land_maintenance_modifier = -0.175
		}
		
		is_bad = no
	}
	
	mechanics = {
	}
	
	ai_will_do = {
		factor = 1
	}
}

estate_acolytes_dominions_law_the_dark_tithe = {
	icon = privilege_dominion_law_dark_tithe
	influence = 0
	loyalty = -0.05
	max_absolutism = 5
	is_valid = {
		tag = Z99
	}
	can_select = {
		always = yes
	}
	can_revoke = {
		is_at_war = no
	}
	on_revoked = {
		every_subject_country = {
			limit = { is_subject_of_type = acolyte_dominion }
			add_liberty_desire = -5
		}
	}
	on_granted = {
		every_subject_country = {
			limit = { is_subject_of_type = acolyte_dominion }
			add_liberty_desire = 10
		}
	}
	penalties = {
		reduced_liberty_desire = -10
	}
	
	benefits = {
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 1
			NOT = { acolyte_dominion = 2 }
		}
		
		modifier = {
			global_tax_modifier = 0.05
		}
		
		is_bad = no
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 2
			NOT = { acolyte_dominion = 3 }
		}
		
		modifier = {
			global_tax_modifier = 0.1
		}
		
		is_bad = no
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 3
			NOT = { acolyte_dominion = 4 }
		}
		
		modifier = {
			global_tax_modifier = 0.15
		}
		
		is_bad = no
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 4
			NOT = { acolyte_dominion = 5 }
		}
		
		modifier = {
			global_tax_modifier = 0.05
		}
		
		is_bad = no
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 5
			NOT = { acolyte_dominion = 6 }
		}
		
		modifier = {
			global_tax_modifier = 0.25
		}
		
		is_bad = no
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 6
			NOT = { acolyte_dominion = 7 }
		}
		
		modifier = {
			global_tax_modifier = 0.3
		}
		
		is_bad = no
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 7
		}
		
		modifier = {
			global_tax_modifier = 0.35
		}
		
		is_bad = no
	}
	
	mechanics = {
	}
	
	ai_will_do = {
		factor = 1
	}
}

estate_acolytes_dominions_law_centralized_banking = {
	icon = privilege_dominion_law_centralized_banking
	influence = -0.05
	loyalty = 0.05
	max_absolutism = 5
	is_valid = {
		tag = Z99
	}
	can_select = {
		always = yes
	}
	can_revoke = {
		is_at_war = no
	}
	on_revoked = {
		every_subject_country = {
			limit = { is_subject_of_type = acolyte_dominion }
			add_liberty_desire = 20
		}
	}
	on_granted = {
		every_subject_country = {
			limit = { is_subject_of_type = acolyte_dominion }
			add_liberty_desire = -15
		}
	}
	penalties = {
	}
	
	benefits = {
		reduced_liberty_desire = 15
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 1
			NOT = { acolyte_dominion = 2 }
		}
		
		modifier = {
			interest = 0.5
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 2
			NOT = { acolyte_dominion = 3 }
		}
		
		modifier = {
			interest = 1
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 3
			NOT = { acolyte_dominion = 4 }
		}
		
		modifier = {
			interest = 1.5
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 4
			NOT = { acolyte_dominion = 5 }
		}
		
		modifier = {
			interest = 2
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 5
			NOT = { acolyte_dominion = 6 }
		}
		
		modifier = {
			interest = 2.5
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 6
			NOT = { acolyte_dominion = 7 }
		}
		
		modifier = {
			interest = 3
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 7
		}
		
		modifier = {
			interest = 3.5
		}
		
		is_bad = yes
	}
	
	mechanics = {
	}
	
	ai_will_do = {
		factor = 1
	}
}

estate_acolytes_dominions_law_power_split = {
	icon = privilege_dominion_law_power_split
	influence = 0
	loyalty = 0.15
	max_absolutism = -5
	is_valid = {
		tag = Z99
	}
	can_select = {
		always = yes
	}
	can_revoke = {
		is_at_war = no
	}
	on_revoked = {
		every_subject_country = {
			limit = { is_subject_of_type = acolyte_dominion }
			add_liberty_desire = 20
		}
	}
	on_granted = {
		every_subject_country = {
			limit = { is_subject_of_type = acolyte_dominion }
			add_liberty_desire = -15
		}
	}
	penalties = {
	}
	
	benefits = {
		reduced_liberty_desire = 10
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 1
			NOT = { acolyte_dominion = 2 }
		}
		
		modifier = {
			all_power_cost = 0.0125
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 2
			NOT = { acolyte_dominion = 3 }
		}
		
		modifier = {
			all_power_cost = 0.025
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 3
			NOT = { acolyte_dominion = 4 }
		}
		
		modifier = {
			all_power_cost = 0.0375
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 4
			NOT = { acolyte_dominion = 5 }
		}
		
		modifier = {
			all_power_cost = 0.05
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 5
			NOT = { acolyte_dominion = 6 }
		}
		
		modifier = {
			all_power_cost = 0.0625
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 6
			NOT = { acolyte_dominion = 7 }
		}
		
		modifier = {
			all_power_cost = 0.075
		}
		
		is_bad = yes
	}
	
	conditional_modifier = {
		trigger = {
			acolyte_dominion = 7
		}
		
		modifier = {
			all_power_cost = 0.0875
		}
		
		is_bad = yes
	}
	
	mechanics = {
	}
	
	ai_will_do = {
		factor = 1
	}
}
