# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
gather_sacrifices_edict = {
	potential = {
		religion = xhazobkult
	}
	
	allow = {
		custom_trigger_tooltip = {
			tooltip = gather_sacrifices_edict_tt
			has_country_flag = xhazobkult_prepare_sacrifices_flag
		}
	}
	
	modifier = {
		local_unrest = 2
	}
	
	color = { 70 125 230 }
	
	
	ai_will_do = {
		factor = 200
		modifier = {
			factor = 0
			NOT = { 
				any_province_in_state = {
					culture_group = gnollish
				}
			}
		}
	}
}
