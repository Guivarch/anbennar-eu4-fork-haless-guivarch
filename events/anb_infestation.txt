namespace = infestation

#  notes for testing: 
#  spawn a random infestation in one of your own provinces with infestation.0
#  find a random infestation globally with infestation.1000
#  find a random infestation locally with infestation.1001
#
#  When adding a new random infestation type, make sure to add it to 
#  infestation.0 or it will never spawn randomly
#  and infestation.2000 to aid in easy cleanup while debugging
#
#  see anb_infestation_*.txt for monsters you can base your designs off of
#  try to use the same event number between different monsters to improve consistency
#  and troubleshooting
#
#  generate a new infestation in the province designated
#  calling this event directly creates a single infestation only
province_event = {
    id = infestation.0
    title = infestation.0.t
    desc = infestation.0.d
    picture = COMET_SIGHTED_eventPicture
    
    is_triggered_only = yes
    hidden = yes

    #mean_time_to_happen = {
	#	days = 31
	#}
    
    trigger = {
        is_city = yes # no infestations in sea tiles, uncolonized land, wasteland
        NOT = { has_province_flag = infestation_present }
		owner = { monstrous_culture = no } #no monsters for now
        has_influencing_fort = no  #forts prevent infestations from spawning if active
    }
    
     #now we go through a list of random monsters we can spawn
     #whenever you create a new monster, add it to this list or it will never
     #randomly spawn

    #  Nothing
    #  this is here so that if no reasonable spawn choice is
    #  available for this province, then nothing will spawn and it 
    #  won't break anything.
    option = {
        ai_chance = { factor = 20 }
    }
	#BROAD DISTRIBUTION INFESTATIONS, use low base chance with regionally higher chances
    #Goblins
    #spawn as infestation in and around serpentspine, but only if it makes
    #sense for them to be considered an infestation
    option = {
        trigger = {
           #  anb scripted racial triggers; gobbos only where gobbos not liked/accepted
            owner = { ruler_is_goblin = no }
            owner = { has_goblin_accepted_culture = no }
            owner = { low_tolerance_goblin_race_trigger = yes }
            NOT = {
                OR = {
                    continent = north_america  #no gobos in aelantir #yet...
                    continent = south_america  #no gobos in aelantir #yet...
					continent = africa #no goblins in Sarhal, ever
                }
            }
        }
        ai_chance = { 
            factor = 1  #low default chance, except...
            modifier = {
                factor = 5
                OR = {
                    superregion = escann_superregion  #due to greentide
                    superregion = deepwoods_superregion  #due to greentide
					continent = serpentspine  #racial homeland
                }
            }
            modifier = {
                factor = 2
                OR = {
                    superregion = rahen_superregion #due to serpentspine proximity
                    superregion = bulwar_superregion  #due to serpentspine proximity
                    superregion = forbidden_lands_superregion  #due to serpentspine proximity
                }
            }
        }
        province_event = { id = infestation_goblin.0 days = 1 }
    }
	#Orcs
	#same distribution as goblins
	 option = {
        trigger = {
           #  anb scripted racial triggers; orcs won't see themselves as an infestation
            owner = { ruler_is_orc = no }
            owner = { has_orcish_accepted_culture = no }
            owner = { low_tolerance_orcish_race_trigger = yes }
            NOT = {
                OR = {
                    continent = north_america  #not found in aelantir in 1444
                    continent = south_america  #not found in aelantir in 1444
					continent = africa #not found in sarhal
                }
            }
        }
        ai_chance = { 
            factor = 1  #low default chance, except...
            modifier = {
                factor = 5
                OR = {
                    superregion = escann_superregion  #due to greentide
                    superregion = deepwoods_superregion  #due to greentide
					continent = serpentspine  #racial homeland
                }
            }
            modifier = {
                factor = 2
                OR = {
                    superregion = rahen_superregion #due to serpentspine proximity
                    superregion = bulwar_superregion  #due to serpentspine proximity
                    superregion = forbidden_lands_superregion  #due to serpentspine proximity
                }
            }
        }
        province_event = { id = infestation_orc.0 days = 1 }
    }
	#Trolls
	#originate in Gerudia, common in Cannor (and future Sarhal region), rare in the rest of Halcann
	 option = {
        trigger = {
           #  anb scripted racial triggers; orcs won't see themselves as an infestation
            owner = { ruler_is_troll = no }
            owner = { has_troll_accepted_culture = no }
            owner = { low_tolerance_troll_race_trigger = yes }
            NOT = {
                OR = {
                    continent = north_america  #not found in aelantir in 1444
                    continent = south_america  #not found in aelantir in 1444
                }
            }
        }
        ai_chance = { 
            factor = 1  #low default chance, except...
            modifier = {
                factor = 5
                OR = {
                    superregion = gerudia_superregion  #racial homeland
                }
            }
            modifier = {
                factor = 2
                OR = {
                    continent = europe #common in Cannor
                }
            }
        }
        province_event = { id = infestation_troll.0 days = 1 }
    }
	#Bandits
	#Miscellaneous outlaws and brigands appearing in devastated provinces
	 option = {
        trigger = {
           devastation = 20
        }
        ai_chance = { 
            factor = 1  #low default chance, except...
            modifier = {
                factor = 2
                devastation = 40
            }
			modifier = {
                factor = 2
                devastation = 60
            }
			modifier = {
                factor = 2
                devastation = 80
            }
			modifier = {
                factor = 2
                devastation = 100
            }
        }
        province_event = { id = infestation_bandit.0 days = 1 }
    }
	#NARROW DISTRIBUTION INFESTATIONS; use higher base chance but stricter triggers
    #Harpies
    #spawn as infestation in and around bulwar, west rahen
    option = {
        trigger = {
            #anb scripted racial triggers; wild harpies only where harpies not liked/accepted
            owner = { ruler_is_harpy = no }
            owner = { has_harpy_accepted_culture = no }
            owner = { low_tolerance_harpy_race_trigger = yes }
            OR = {
                superregion = bulwar_superregion #racial homeland
                region = rakhadesh_region  #western Rahen
                region = serpent_gift_region  #through mountain pass; centaurs here
                area = irdu_ageeneas_area  #through mountain pass; centaurs here 
            }
        }
        ai_chance = { 
            factor = 4 # medium chance; slightly more common than goblins in these regions
        }
        province_event = { id = infestation_harpy.0 days = 1 }
    }
	#Gnolls
	#found in the salahad, near the khenak mountains and in the Folly
	option = {
        trigger = {
            #anb scripted racial triggers; wild harpies only where harpies not liked/accepted
            owner = { ruler_is_gnoll = no }
            owner = { has_gnollish_accepted_culture = no }
            owner = { low_tolerance_gnollish_race_trigger = yes }
            OR = {
                superregion = bulwar_superregion #racial homeland
				superregion = salahad_superregion #racial homeland
                region = daravans_folly_region  #flamemarked
				area = khenak_area #hillthrone
				area = inner_khenak_area #hillthrone
				area = mountainway_area #hillthrone
				area = gnollsgate_area #hillthrone
            }
			NOT = {
				province_id = 451 #nathalaire is an island
				province_id = 376 #stingport is an island
			}
        }
        ai_chance = { 
            factor = 4
        }
        province_event = { id = infestation_gnoll.0 days = 1 }
    }
	#Werewolves
	#found in the Greatwoods, possibly Deepwoods
	option = {
        trigger = {
            OR = {
				province_group = greatwoods_province_group
            }
        }
        ai_chance = { 
            factor = 3
			modifier = {
                factor = 2
                has_terrain = woods
            }
			modifier = {
                factor = 3
                has_terrain = forest
            }
        }
        province_event = { id = infestation_werewolf.0 days = 1 }
    }
	#Hags
	#found in the Greatwoods, future in Sarhal
	option = {
        trigger = {
            OR = {
				province_group = greatwoods_province_group
            }
        }
        ai_chance = { 
            factor = 2
			modifier = {
                factor = 2
                has_terrain = marsh
            }
        }
        province_event = { id = infestation_hag.0 days = 1 }
    }
	#Bugbears
	#found near Hobgoblins
	option = {
        trigger = {
            OR = {
				region = shamakhad_region
				region = jade_mines_region
            }
        }
        ai_chance = { 
            factor = 4
        }
        province_event = { id = infestation_bugbear.0 days = 1 }
    }
	#Cultists
	#found in devastated Cannorian provinces, basically worse bandits
	option = {
        trigger = {
            OR = {
				current_age = age_of_reformation
				current_age = age_of_absolutism
            }
			religion_group = cannorian
			devastation = 20
        }
        ai_chance = { 
            factor = 4
        }
        province_event = { id = infestation_cultist.0 days = 1 }
    }
	#Satyrs
	#found in Deepwoods
	option = {
        trigger = {
            OR = {
				superregion = deepwoods_superregion #racial homeland
				region = domandrod_region
			}
        }
        ai_chance = { 
            factor = 4
        }
        province_event = { id = infestation_satyr.0 days = 1 }
    }
	#Kobolds
	#found in Dragon Coast
	option = {
        trigger = {
            region = dragon_coast_region #racial homeland
        }
        ai_chance = { 
            factor = 4
        }
        province_event = { id = infestation_kobold.0 days = 1 }
    }
	#Ogres
	#found near Serpentspine, rare
	option = {
        trigger = {
             OR = {
				region = northern_pass_region
				region = harpy_hills_region
				region = serpents_vale_region
                superregion = forbidden_lands_superregion
				superregion = rahen_superregion
				superregion = yanshen_superregion
			}
        }
        ai_chance = { 
            factor = 1
        }
        province_event = { id = infestation_ogre.0 days = 1 }
    }
	#SPECIAL INFESTATIONS; unique or particularly bespoke
    # Dragons
    # Named Dragon: Merathis (Cannor)
    # option = {
    #     trigger = {
    #         is_year = 1522  #picked arbitrarily to push this content later into the game
    #         NOT = { has_global_flag = infestation_dragon_merathis_spawned }
    #         continent = europe  #merathis is a cannorian dragon
    #         owner = {
    #             ai = no #spawn this only for players
    #         }
    #     }
    #     ai_chance = {
    #         factor = 3 # give it a higher base chance because it will only spawn for players 
    #         modifier = {
    #             factor = 3
    #             OR = {
    #                 has_terrain = mountain
    #                 has_terrain = highlands
    #                 has_terrain = cavern
    #                 has_terrain = dwarven_hold
    #                 has_terrain = dwarven_hold_surface
    #             }
    #         }
    #         modifier = { #more likely to spawn for a player with an ongoing infestation
    #             factor = 3
    #             owner = {
    #                 ai = no
    #                 any_owned_province = {
    #                     has_province_flag = infestation_present
    #                 }
    #             }
    #         }
    #     }
    #     set_global_flag = infestation_dragon_merathis_spawned
    #     province_event = { id = infestation_dragon_merathis.0 days = 1 }
    # }
   
   # Undead
   # Zombies (for apocalype) - 
    option = {
        trigger = {
            is_year = 1488  #picked arbitrarily to push this content back from game start
            NOT = { has_global_flag = infestation_zombie_apocalypse_enabled }
        }
        ai_chance = { 
            factor = 1
            modifier = {
                factor = 0.1
                has_global_flag = infestation_zombie_apocalypse_happened
            }
            modifier = {
                factor = 2
                estate_influence = {
                    estate = estate_mages
                    influence = 50
                }
            }
            modifier = {
                factor = 3
                has_building = mage_tower
            }
            modifier = {
                factor = 5
                owner = {
                    OR = {
                        AND = {
                            personality = mage
                            OR = { 
                                has_ruler_flag = necromancy_1
                                has_ruler_flag = necromancy_2
                                has_ruler_flag = necromancy_3
                            }
                        }
                        AND = {
                            heir_has_personality = mage
                            OR = { 
                                has_ruler_flag = necromancy_1
                                has_ruler_flag = necromancy_2
                                has_ruler_flag = necromancy_3
                            }
                        }
                    }
                }
            }
        }
        province_event = { id = infestation_zombie.0 days = 1 }
    }
    
 }


  #pulses every month to put a monster on the map

  #shit fixed
province_event = {
    id = infestation.1
    title = infestation.1.t
    desc = infestation.1.d
    picture = COMET_SIGHTED_eventPicture
   
    is_triggered_only = yes
    hidden = yes
    
    immediate = {
        #find us a random province to spawn out monster
        random_province = {
            limit = {
                is_city = yes # no infestations in sea tiles, uncolonized land, wasteland
                NOT = { has_province_flag = infestation_present }
            }
            save_event_target_as = infestation_target
        }
     }
    
    option = {
        ai_chance = { factor = 100 }
        hidden_effect = {
            event_target:infestation_target = {
				#province_event = { id = infestation.1 days = 30 random = 5 } #recursion is bad mkay
				province_event = { id = infestation.0 } # spawn a monster here
            }
       }
    }
 }

#  set up our event chain by abusing an existing country with a known colonized province
#  524 is owned by magesterium, tag A85, in 1444, and should always be owned by someone
#  we do it this way so that the check only happens once at game start
#  after which every subsequent event is scoped to a single province
 #country_event = {
 #    id = infestation.2
 #    title = infestation.2.t
 #    desc = infestation.2.d
 #    picture = COMET_SIGHTED_eventPicture
 #   
 #    fire_only_once = yes
 #    hidden = yes
 #    major = yes
 #    is_triggered_only = no
 #   
 #    mean_time_to_happen = { 
 #        days = 1
 #    }
 #    trigger = {
 #        owns = 524
 #    }
 #   
 #    immediate = {
 #        random_province = {
 #            limit = {
 #                is_city = yes # no infestations in sea tiles, uncolonized land, wasteland
 #                NOT = { has_province_flag = infestation_present }
 #            }
 #            save_event_target_as = infestation_target
 #        }
 #    }
 #    option = {
 #        ai_chance = { factor = 100 }
 #        hidden_effect = {
 #            event_target:infestation_target = {
 #                province_event = { id = infestation.1 days = 30 random = 5 }
 #            }
 #        }
 #    }
 #}
 
    
    

# a debug event, to find province where infestation has spawned
# allowing you to tag switch to them and play it out
country_event = {
    id = infestation.1000
    title = infestation.1000.t
    desc = infestation.1000.d
    picture = COMET_SIGHTED_eventPicture
    
    is_triggered_only = yes
    
    mean_time_to_happen = {
        days = 1
    }
    
    trigger = {
    
    }
    
    immediate = {
        random_province = {
            limit = {
                has_province_flag = infestation_present
            }
            save_event_target_as = infested_target1
        }
        random_province = {
            limit = {
                has_province_flag = infestation_present
            }
            save_event_target_as = infested_target2
        }        
        random_province = {
            limit = {
                has_province_flag = infestation_present
            }
            save_event_target_as = infested_target3
        }
        random_province = {
            limit = {
                has_province_flag = infestation_present
            }
            save_event_target_as = infested_target4
        }
        random_province = {
            limit = {
                has_province_flag = infestation_present
            }
            save_event_target_as = infested_target5
        }
    }
    option = {
        name = infestation.1000.a
        goto = infested_target1
    }
    option = {
        name = infestation.1000.a
        goto = infested_target2
    }
    option = {
        name = infestation.1000.a
        goto = infested_target3
    }
    option = {
        name = infestation.1000.a
        goto = infested_target4
    }
    option = {
        name = infestation.1000.a
        goto = infested_target5
    }
}

# same debug event, limited to owned provinces, to find province where infestation has spawned
country_event = {
    id = infestation.1001
    title = infestation.1001.t
    desc = infestation.1001.d
    picture = COMET_SIGHTED_eventPicture
    
    is_triggered_only = yes
   
    mean_time_to_happen = {
        days = 1
    }
   
    trigger = {
   
    }
    
    immediate = {
        random_owned_province = {
            limit = {
                has_province_flag = infestation_present
            }
            save_event_target_as = infested_target1
        }
        random_owned_province = {
            limit = {
                has_province_flag = infestation_present
            }
            save_event_target_as = infested_target2
        }        
        random_owned_province = {
            limit = {
                has_province_flag = infestation_present
            }
            save_event_target_as = infested_target3
        }
        random_owned_province = {
            limit = {
                has_province_flag = infestation_present
            }
            save_event_target_as = infested_target4
        }
        random_owned_province = {
            limit = {
                has_province_flag = infestation_present
            }
            save_event_target_as = infested_target5
        }
    }
    option = {
        name = infestation.1001.a
        goto = infested_target1
    }
    option = {
        name = infestation.1001.a
        goto = infested_target2
    }
    option = {
        name = infestation.1001.a
        goto = infested_target3
    }
    option = {
        name = infestation.1001.a
        goto = infested_target4
    }
    option = {
        name = infestation.1001.a
        goto = infested_target5
    }
}

#  cleanup event - call for any reason on any province to remove all trace of infestation
#  when creating a new infestation type, make sure you add its cleanup event to this list
province_event = {
    id = infestation.2000
    title = infestation.2000.t
    desc = infestation.2000.d
    picture = COMET_SIGHTED_eventPicture
   
    is_triggered_only = yes
    hidden = yes
   
    option = {
        ai_chance = { factor = 100 }
        province_event = { id = infestation_goblin.2000 }
        province_event = { id = infestation_dragon_merathis.2000 }
        province_event = { id = infestation_zombie.2000 }
        province_event = { id = infestation_harpy.2000 }
		province_event = { id = infestation_bandit.2000 }
		province_event = { id = infestation_cultist.2000 }
		province_event = { id = infestation_gnoll.2000 }
		province_event = { id = infestation_hag.2000 }
		province_event = { id = infestation_kobold.2000 }
		province_event = { id = infestation_ogre.2000 }
		province_event = { id = infestation_orc.2000 }
		province_event = { id = infestation_troll.2000 }
		province_event = { id = infestation_werewolf.2000 }
    }
}

#testing
# country_event = {
    # id = infestation.2001
    # title = infestation.2000.t
    # desc = infestation.2000.d
    # picture = COMET_SIGHTED_eventPicture
   
    # trigger = {
       # OR = {
			# capital_scope = { region = dalr_region }
			# capital_scope = { region = gerudian_coast_region }
		# }
	# }
		
	# mean_time_to_happen = { 
		# days = 7
	# }
	
	# option = {
		# random_owned_province = {
            # limit = {
                # is_city = yes # no infestations in sea tiles, uncolonized land, wasteland
				# owned_by = ROOT
				# NOT = { has_province_flag = infestation_present }
            # }
            # province_event = { id = infestation.0 days = 1 }
        # }
    # }
# }